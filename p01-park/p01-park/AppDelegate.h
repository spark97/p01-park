//
//  AppDelegate.h
//  p01-park
//
//  Created by Seoyoon Park on 1/28/16.
//  Copyright © 2016 Seoyoon Park. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

