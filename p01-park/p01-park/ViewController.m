//
//  ViewController.m
//  p01-park
//
//  Created by Seoyoon Park on 1/28/16.
//  Copyright © 2016 Seoyoon Park. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize helloWorld, button;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onPress:(id)sender{
    [helloWorld setText:@"Seoyoon Park"];
    [helloWorld setTextColor:[UIColor redColor]];
    [UIView animateWithDuration:1 animations:^{
        button.backgroundColor = [UIColor blueColor];
    }];
}

@end
