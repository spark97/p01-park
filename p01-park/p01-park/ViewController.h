//
//  ViewController.h
//  p01-park
//
//  Created by Seoyoon Park on 1/28/16.
//  Copyright © 2016 Seoyoon Park. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *helloWorld;
@property (nonatomic, strong) IBOutlet UIButton *button;

-(IBAction)onPress:(id)sender;

@end

